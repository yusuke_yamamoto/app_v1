User.create!(name:  "owner",
             email: "yusuke.yamamoto.2014@gmail.com",
             password:              "hogehoge",
             password_confirmation: "hogehoge",
             admin: true)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

__END__
Update.create!(
title:"ページの作成をはじめました",
content:"まだまだ研究中ですが、サイトの構築をスタートしております。"
)
Update.create!(
title:"最新の更新をお知らせできるようになりました",
content:"最新のお知らせを個別ページおよびトップにて3件見れるようにしました。"
)
Update.create!(
title:"テスト",
content:"テストです"
)
Item.create!(
title:"黒博物館 スプリンガルド",
description:"「うしおととら」「からくりサーカス」の著者による短編。黒博物館シリーズの１作目であり、近代ロンドンを舞台に彼の描写力が光る一品。	",
price:500,
author:"藤田 和日郎",
company:"講談社",
type:2,
category:3,
sold_from:"2018-05-09",
photo:
)
Item.create!(
title:"シラノ・ド・ベルジュラック",
description:"実在の人物「シラノ・ド・ベルジュラック」を主人公にした戯曲。全文の言葉選びが秀逸であり、もしフランス語に堪能であれば、言語での読書をオススメしたい。	",
price:500,
author:"エドモン ロスタン",
company:"光文社",
type:1,
category:4,
sold_from:"2018-05-09",
photo:
)
Item.create!(
title:"青色本",
description:"ウィトゲンシュタインが行った講義録を元に作成された中期研究の必読書。如何にして彼が「語り得ぬもの」にたどり着いたのかを知ることができる。	",
price:500,
author:"ルードヴィヒ・ウィトゲンシュタイン",
company:"講談社",
type:1,
category:1,
sold_from:"2018-05-09",
photo:"http://t1.gstatic.com/images?q=tbn:ANd9GcR9pAHqTEqim6Q59W0_GdbWw1AdjorwSnBZP90t2KrKrVcqOi51"
)
Review.create!(
content:"青色本を読んだのは、「論理哲学論考」を読んだ後であった。かの素晴らしい本は、完成されている一方で、人間らしい思考のかけた「論考」出会ったように思う。一方で、青色本はそこに辿り着くためのウィトゲンシュタインの人間らしい苦悩である。この本を読むことで、なぜ語りえぬものには沈黙せざるを得ないのかが読者にも痛感できるだろう。",
user:1,
item:1,
point:5,
)
Review.create!(
content:"一冊にまとまった内容としては最高峰の漫画であると思う。近代イギリスという現代との境目の時代を、見事に描写し、ふとおかしく思えるようなからくり仕掛けも、フィクションと現実の間で上手く描けている。まるでシャーロックホームズが歴史上実在したと思う人がいるように、この漫画も滑稽なのに現実のような感覚を呼び起こしてくれる。",
user:1,
item:2,
point:4
)
Review.create!(
content:"この本は演劇である。本でありながら、2時間ばかりで読める軽さに、その軽快なセリフは素晴らしい演劇でしかない。醜男のシラノを、醜いままにあんなにカッコいい男として描くのは、冒険活劇でありロマンスである。翻訳を含めて、素晴らしいの一言。",
user:1,
item:3,
point:5
)
