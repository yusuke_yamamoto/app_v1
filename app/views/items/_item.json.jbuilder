json.extract! item, :id, :title, :description, :price, :author, :company, :type_id, :category_id, :sold_from, :created_at, :updated_at
json.url item_url(item, format: :json)
