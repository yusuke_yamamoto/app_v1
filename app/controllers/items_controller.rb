class ItemsController < ApplicationController
  before_action :set_item, only: [:show, :edit, :update, :destroy]

  # GET /items
  # GET /items.json
  def index
    @items = Item.paginate(page: params[:page])
  end

  def typeahead_action
        render json: Item.select("#{params[:title]}").where(Item.arel_table["#{params[:title]}".to_sym].matches("%#{params[:term]}%")).uniq
  end

  # GET /items/1
  # GET /items/1.json
  def show
    @item = Item.find(params[:id])
    @reviews = @item.reviews.paginate(page: params[:page])
    if logged_in?
      @review = current_user.reviews.build
    else
      @review = Review.new
    end

  end

  # GET /items/new
  def new
    @item = Item.new
  end

  # GET /items/1/edit
  def edit
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(item_params)

    respond_to do |format|
      if @item.save
        if session[:review]
          session[:review][:item_id] = Item.last.id
            if logged_in? && session[:review][:user_id] != "5"
              @review = current_user.reviews.build(session[:review])
            else
              @review = User.find(5).reviews.build(session[:review])
            end

            if @review.save
              flash[:success] = "レビューが作成されました"
              redirect_to root_url
            else
              @feed_items = []
              render 'static_pages/home' and return
            end
        end
        end
      else
        format.html { render :new } and return
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.json
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.json { render :show, status: :ok, location: @item }
      else
        format.html { render :edit }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_item
      @item = Item.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:title, :description, :price, :author, :company, :type_id, :category_id, :sold_from, :photo)
    end
