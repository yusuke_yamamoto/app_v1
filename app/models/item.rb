class Item < ApplicationRecord
  has_many :reviews
  belongs_to :type
  belongs_to :category
end
